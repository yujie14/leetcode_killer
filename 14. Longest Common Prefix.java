class Solution {
    public String longestCommonPrefix(String[] strs) 
    {
        if(strs.length == 0)
            return "";
        int min = Integer.MAX_VALUE;
        for(int i = 0; i < strs.length; i++)
            min = Math.min(min, strs[i].length());
        return helper(strs, 0, min);
    }
    public String helper(String[] strs, int count, int min)
    {
        if(min <= count)
            return "";
        char c = strs[0].charAt(count);
        for(int i = 0; i < strs.length; i++)
            if(c != strs[i].charAt(count))
                return "";
        return c + helper(strs, count+1, min);    
    }
}