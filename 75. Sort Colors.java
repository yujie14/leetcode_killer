class Solution {
    public void sortColors(int[] nums) 
    {
        int[] count = new int[3];
        for(int i = 0; i < nums.length; i++)
            count[nums[i]]++;
        int num = 0;
        for(int i = 0; i < count.length; i++)
            for(int j = 0; j < count[i]; j++)
                nums[num++] = i;
    }
}