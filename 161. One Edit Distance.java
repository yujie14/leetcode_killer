class Solution {
    public boolean isOneEditDistance(String s, String t) 
    {
        boolean flag = false;
        int l1 = s.length();
        int l2 = t.length();
        if(Math.abs(l1 - l2) > 1)
            return false;
        int i = 0;
        int j = 0;
        while(i < l1 && j < l2 && s.charAt(i) == t.charAt(j))
        {
            i++;
            j++;
        }
        if(i == s.length() && j == t.length())
            return false;
        if(Math.abs(l1 - i) <= 1 && Math.abs(l2 - j) <= 1)
            return true;
        if(i == l1 || j == l2)
            return false;
        return isSame(s, t, i, j+1) || isSame(s, t, i+1, j) || isSame(s, t, i+1, j+1);
        
    }
    public boolean isSame(String s, String t, int i, int j)
    {
        int c = 0;
        while(i < s.length() && j < t.length() && s.charAt(i) == t.charAt(j))
        {
            i++;
            j++;
        }
        if(i == s.length() && j == t.length())
            return true;
        return false;
    }
}