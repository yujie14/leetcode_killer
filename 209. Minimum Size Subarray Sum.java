class Solution {
    public int minSubArrayLen(int s, int[] nums) 
    {
        if(nums.length == 0)
            return 0;
        int min = Integer.MAX_VALUE;
        int sum = nums[0];
        int i = 0;
        int j = 0;
        while(true)
        {
            while(sum < s && j < nums.length-1)
                sum = sum + nums[++j];
            if(j == nums.length-1 && sum < s)
                break;
            while(sum >= s)
            {
                min = Math.min(min, j-i+1);
                sum = sum - nums[i++];
            }
        }
        return (min==Integer.MAX_VALUE)?0:min;
    }
}