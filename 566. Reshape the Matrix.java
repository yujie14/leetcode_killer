class Solution {
    public int[][] matrixReshape(int[][] nums, int r, int c) 
    {
        if(nums.length * nums[0].length != r * c)
            return nums;
        int m = nums.length;
        int n = nums[0].length;
        int[][] result = new int[r][c];
        int count1 = 0;
        int count2 = 0;
        for(int i = 0; i < r; i++)
            for(int j = 0; j < c; j++)
            {
                result[i][j] = nums[count1][count2];
                count2++;
                if(count2 == n)
                {
                    count2 = 0;
                    count1++;
                }
            }
        return result;
    }
}