class Solution {
    public void gameOfLife(int[][] board) 
    {
        int count = 0;
        int[][] temp = new int[board.length+2][board[0].length+2];
        for(int i = 0; i < board.length; i++)
            for(int j = 0; j < board[0].length; j++)
                temp[i+1][j+1] = board[i][j];
        for(int i = 0; i < board.length; i++)
            for(int j = 0; j < board[0].length; j++)
            {
                count = 0;
                for(int p = 0; p < 3; p++)
                    for(int q = 0; q < 3; q++)
                        if((p != 1 || q != 1) && temp[i+p][j+q] == 1)
                            count++;
                if(board[i][j] == 1 && (count == 2 || count == 3))
                    board[i][j] = 1;
                else if(count == 3)
                    board[i][j] = 1;
                else
                    board[i][j] = 0;
            }
    }
}