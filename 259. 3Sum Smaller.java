class Solution {
    public int threeSumSmaller(int[] nums, int target) 
    {
        List<List<Integer>> result = new ArrayList();
        helper(result, new ArrayList(), target, 0, 0, nums);
        return result.size();
    }
    public void helper(List<List<Integer>> result, List temp, int target, int sum, int start, int[] nums)
    {
        if(temp.size() == 3)
        {
            if(sum < target)
                result.add(new ArrayList(temp));
            else;
        }
        else
            for(int i = start; i < nums.length; i++)
            {
                temp.add(nums[i]);
                helper(result, temp, target, sum + nums[i], i + 1, nums);
                temp.remove(temp.size() - 1);
            }
    }
}