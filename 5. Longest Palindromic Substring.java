class Solution {
    public String longestPalindrome(String s) 
    {
        if(s.length() == 0)
            return s;
        int left = 0;
        int right = 0;
        int max = 0;
        int count = 0;
        for(int i = 0; i < s.length() - 1; i++)
        {
            count = Math.max(helper(s, i, i), helper(s, i, i+1));
            if(count > max)
            {
                max = count;
                left = i - (max-1)/2;
                right = i + max/2;
            }
        }
        return s.substring(left, right+1);
        
        
    }
    public int helper(String s, int i, int j)
    {
        while(i >= 0 && j < s.length() && s.charAt(i) == s.charAt(j))
        {
            i--;
            j++;
        }
        return j - i - 1;
    }
}