class Solution {
    public int maxProduct(int[] nums) 
    {
        int min = 1;
        int max = 1;
        int result = Integer.MIN_VALUE;
        for(int i = 0; i < nums.length; i++)
        {
            int tep = max;
            max = Math.max(Math.max(max*nums[i], min*nums[i]), nums[i]);
            min = Math.min(Math.min(tep*nums[i], min*nums[i]), nums[i]);
            result = Math.max(max, result);
        }
        return result;
    }
}