/**
 * Definition for an interval.
 * public class Interval {
 *     int start;
 *     int end;
 *     Interval() { start = 0; end = 0; }
 *     Interval(int s, int e) { start = s; end = e; }
 * }
 */
class Solution {
    private class IntergalComparator implements Comparator<Interval>
    {
        public int compare(Interval a, Interval b)
        {
            if(a.start < b.start)
                return -1;
            else if(a.start == b.start)
                return 0;
            else
                return 1;
        }
    }
    
    public List<Interval> merge(List<Interval> intervals) 
    {
        List<Interval> result = new ArrayList();
        if(intervals.size() == 0)
            return result;
        Collections.sort(intervals, new IntergalComparator());
        int start = intervals.get(0).start;
        int end = intervals.get(0).end;
        for(Interval i: intervals)
        {
            if(end < i.start)
            {
                result.add(new Interval(start, end));
                start = i.start;
                end = i.end;
            }
            else if(end >= i.start && end < i.end)
                end = i.end;
            else if(end >= i.end);
        }
        result.add(new Interval(start, end));
        return result;
    }
}