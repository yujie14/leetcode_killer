class Solution {
    public int arrayPairSum(int[] nums) 
    {
        int count = 0;
        Arrays.sort(nums);
        for(int i = 0; i < nums.length; i = i+2)
            count = count + nums[i];
        return count;
    }
}