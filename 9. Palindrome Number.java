class Solution {
    public boolean isPalindrome(int x) 
    {
        if(x < 0)
            return false;
        int result = 0;
        int y = x;
        while(x != 0)
        {
            result = 10 * result + x%10;
            x = x/10;
        }
        return result==y;
    }
}