class Solution {
    public List<List<Integer>> subsets(int[] nums) 
    {
        List<List<Integer>> result = new ArrayList();
        for(int i = 0; i <= nums.length; i++)
            helper(result, new ArrayList(), nums, i, 0);
        return result;
    }
    public void helper(List<List<Integer>> result, List temp, int[] nums, int all, int start)
    {
        if(temp.size() == all)
            result.add(new ArrayList(temp));
        else
        {
            for(int i = start; i < nums.length; i++)
            {
                temp.add(nums[i]);
                helper(result, temp, nums, all, i + 1);
                temp.remove(temp.size() - 1);
            }
        }
    }
}