class Solution {
    public int[] productExceptSelf(int[] nums) 
    {
        int[] result = new int[nums.length];
        boolean flag = false;
        long product = 1;
        int index = -1;
        for(int i = 0; i < nums.length; i++)
        {
            if(nums[i] != 0)
                product = product * nums[i];
            else if(nums[i] == 0 && !flag)
            {
                flag = true;
                index = i;
            }
            else if(nums[i] == 0 && flag)
                return result;
        }
        if(flag)
        {
            result[index] = (int)product;
            return result;
        }
        for(int i = 0; i < nums.length; i++)
            result[i] = (int)(product/nums[i]);
        return result;
    }
}