class Solution {
    public boolean wordPattern(String pattern, String str) 
    {
        String[] words = str.split(" ");
        Map<Character, String> map = new HashMap();
        if(words.length != pattern.length())
            return false;
        int i=0;
        for(char c:pattern.toCharArray())
        {
            if(map.containsKey(c))
            {
                if(!(map.get(c)).equals(words[i]))
                    return false;
            }
            else if(map.containsValue(words[i]))
                return false;
            else 
                map.put(c, words[i]);
            i++;
        }
        return true;
    }
}