class Solution {
    String[] num ={"", "", "abc", "def", "ghi", "jkl", "mno", "pqrs", "tuv", "wxyz"};
    public List<String> letterCombinations(String digits) 
    {
        if(digits.length() == 0)
            return new ArrayList();
        List<String> result = new ArrayList();
        helper(result, "", digits, 0);
        return result;
    }
    public void helper(List result, String s, String digits, int n)
    {
        if(digits.length() == n)
            result.add(s);
        else
        {
            String str = num[digits.charAt(n) - '0'];
            for(int i = 0; i < str.length(); i++)
                helper(result, s + str.charAt(i), digits, n + 1);
        }
    }
}