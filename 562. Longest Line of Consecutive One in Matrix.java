class Solution {
    int result = 0;
    public int longestLine(int[][] M) 
    {
        if(M.length == 0 || M[0].length == 0)
            return 0;
        int m = M.length;
        int n = M[0].length;
        int[][][] len = new int[m+1][n+1][4];
        for(int i = 1; i < len.length; i++)
            for(int j = 1; j < len[0].length; j++)
                if(M[i-1][j-1] == 1)
                {
                    len[i][j][0] = len[i-1][j][0] + 1;
                    len[i][j][1] = len[i][j-1][1] + 1;
                    len[i][j][2] = len[i-1][j-1][2] + 1;
                    if(j < n)
                        len[i][j][3] = len[i-1][j+1][3] + 1;
                    else
                        len[i][j][3] = 1;
                    for(int k = 0; k < 4; k++)
                        result = Math.max(result, len[i][j][k]);
                }
        return result;
    }
}