/**
 * Definition for singly-linked list.
 * public class ListNode {
 *     int val;
 *     ListNode next;
 *     ListNode(int x) { val = x; }
 * }
 */
class Solution {
    public ListNode removeNthFromEnd(ListNode head, int n) 
    {
        ListNode start = new ListNode(0);
        start.next = head;
        ListNode a = start;
        int count = 0;
        while(a.next != null)
        {
            a = a.next;
            count++;
        }
        a = start;
        for(int i = 0; i < count - n; i++)
            a = a.next;
        a.next = a.next.next;
        return start.next;
    }
}