class Solution {
    public int threeSumClosest(int[] nums, int target) 
    {
        Arrays.sort(nums);
        int minidiff = Integer.MAX_VALUE;
        int close_sum = -1;
        for(int i = 0; i < nums.length; i++)
        {
            int left = i + 1;
            int right = nums.length - 1;
            while(left < right)
            {
                int sum = nums[left] + nums[right];
                int new_target = target - nums[i];
                if(sum < new_target)
                {
                    if(minidiff > new_target - sum)
                    {
                        minidiff = new_target - sum;
                        close_sum = sum + nums[i];
                    }
                    left++;
                    continue;
                }
                if(sum > new_target)
                {
                    if(minidiff > sum - new_target)
                    {
                        minidiff = sum - new_target;
                        close_sum = sum + nums[i];
                    }
                    right--;
                    continue;
                }
                return sum + nums[i];
            }
        }
        return close_sum;
    }
}