class Solution {
    public boolean searchMatrix(int[][] matrix, int target) 
    {
        if(matrix.length == 0 || matrix[0].length == 0 || target < matrix[0][0])
            return false;
        int i = 0;
        while(i < matrix.length && target >= matrix[i][0])
            i++;
        i--;
        for(int j = 0; j < matrix[i].length; j++)
            if(matrix[i][j] == target)
                return true;
        return false;
    }
}