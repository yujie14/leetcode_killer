class Solution {
    public List<List<Integer>> combinationSum(int[] candidates, int target) 
    {
        Arrays.sort(candidates);
        List result = new ArrayList();
        helper(result, new ArrayList(), candidates, target, 0);
        return result;
    }
    public void helper(List result, List temp, int[] candidates, int target, int start)
    {
        if(target == 0)
            result.add(new ArrayList(temp));
        else if(target < 0);
        else
            for(int i = start; i < candidates.length; i++)
            {
                temp.add(candidates[i]);
                helper(result, temp, candidates, target - candidates[i], i);
                temp.remove(temp.size() - 1);
            }
    }
}