class Solution {
    public int myAtoi(String str) 
    {
        str = str.trim();
        boolean flag = false;
        int result = 0;
        for(int i = 0; i < str.length(); i++)
        {
            if(i==0 && str.charAt(i) == '-')
                flag = true;
            else if(i==0 && str.charAt(i) == '+')
                flag = false;
            else if(!Character.isDigit(str.charAt(i)))
                break;
            else if(Character.isDigit(str.charAt(i)))
            {
                int digit = (str.charAt(i)-'0');
                if (result > (Integer.MAX_VALUE-digit)/10) 
                    return flag ? Integer.MIN_VALUE:Integer.MAX_VALUE;
                result = result*10 + digit;
            }
        }
        return (flag?(-1):1) * result;
    }
}