class Solution {
    public int[] searchRange(int[] nums, int target) 
    {
        int left = 0;
        int right = nums.length - 1;
        int index = -1;
        while(left <= right)
        {
            int medium = (left + right) / 2;
            if(nums[medium] == target)
            {
                index = medium;
                break;
            }
            if(target > nums[medium])
                left = medium + 1;
            else
                right = medium - 1;
        }
        int a = index;
        int b = index;
        if(index == -1)
            return new int[]{-1, -1};
        while(a >= 0 && nums[a] == nums[index])
            a--;
        while(b < nums.length && nums[b] == nums[index])
            b++;
        return new int[]{a+1, b-1};
    }
}