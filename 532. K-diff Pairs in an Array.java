class Solution {
    public int findPairs(int[] nums, int k) 
    {
        int count = 0;
        Arrays.sort(nums);
        for(int i = 0; i < nums.length; i++)
        {
            if(i != 0 && nums[i-1] == nums[i])
                continue;
            int temp = i + 1;
            while(temp < nums.length && nums[temp] < nums[i] + k)
                temp++;
            if(temp < nums.length && nums[temp] == nums[i] + k)
                count++;
        }
        return count;
    }
}