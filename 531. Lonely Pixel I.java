class Solution {
    public int findLonelyPixel(char[][] picture) 
    {
        int count = 0;
        if(picture.length == 0 || picture[0].length == 0)
            return 0;
        int m = picture.length;
        int n = picture[0].length;
        int[] row = new int[m];
        int[] col = new int[n];
        for(int i = 0; i < m; i++)
            for(int j = 0; j < n; j++)
                if(picture[i][j] == 'B')
                {
                    row[i]++;
                    col[j]++;
                }
        for(int i = 0; i < m; i++)
            for(int j = 0; j < n; j++)
                if(picture[i][j] == 'B' && row[i] == 1 && col[j] == 1)
                    count++;
        return count;
    }
}