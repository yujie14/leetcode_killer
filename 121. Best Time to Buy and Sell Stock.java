class Solution {
    public int maxProfit(int[] prices) 
    {
        if(prices.length == 0)
            return 0;
        int price = 0;
        int min = Integer.MAX_VALUE;
        for(int i = 0; i < prices.length; i++)
        {
            min = Math.min(min, prices[i]);
            price = Math.max(price, prices[i] - min);
        }
        return price;
    }
}