class Solution {
    public int uniquePathsWithObstacles(int[][] obstacleGrid) 
    {
        int m = obstacleGrid.length;
        int n = obstacleGrid[0].length;
        int[][] p = new int[m][n];
        for(int i = 0; i < m; i++)
            for(int j = 0; j < n; j++)
            {
                if(i == 0 && j == 0)
                    p[i][j] = (obstacleGrid[i][j]==1)?0:1;
                else if(i == 0)
                    p[i][j] = (obstacleGrid[i][j-1]==1)?0:p[i][j-1];
                else if(j == 0)
                    p[i][j] = (obstacleGrid[i-1][j]==1)?0:p[i-1][j];
                else
                    p[i][j] = ((obstacleGrid[i][j-1]==1)?0:p[i][j-1]) + ((obstacleGrid[i-1][j]==1)?0:p[i-1][j]);
            }
        return (obstacleGrid[m-1][n-1]==1)?0:p[m-1][n-1];
    }
}