class Solution {
    public int search(int[] nums, int target) 
    {
        if (nums == null || nums.length == 0) 
            return -1;
        int start = 0, end = nums.length - 1;
        while(start + 1 < end)
        {
            int medium = (start + end) / 2;
            if(nums[medium] == target)
                return medium;
            if(nums[start] < nums[medium])
            {
                if(target >= nums[start] && target < nums[medium])
                    end = medium;
                else
                    start = medium;
            }
            else
            {
                if(target > nums[medium] && target < nums[start])
                    start = medium;
                else
                    end = medium;
            }
        }
        if (nums[start] == target) 
            return start;
        if (nums[end] == target) 
            return end;
        return -1;
    }
}