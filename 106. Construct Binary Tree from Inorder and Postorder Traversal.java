/**
 * Definition for a binary tree node.
 * public class TreeNode {
 *     int val;
 *     TreeNode left;
 *     TreeNode right;
 *     TreeNode(int x) { val = x; }
 * }
 */

class Solution {
    public int search(int[] arr, int target)
    {
        for(int i = 0; i < arr.length; i++)
            if(arr[i] == target)
                return i;
        return -1;
    }
    public TreeNode buildTree(int[] inorder, int[] postorder) 
    {
        if(postorder.length == 0 || inorder.length == 0)
            return null;
        int index = search(inorder, postorder[postorder.length-1]);
        TreeNode t = new TreeNode(postorder[postorder.length-1]);
        if(index == -1)
            return null;
        if(index != 0)
            t.left=buildTree(Arrays.copyOfRange(inorder,0,index), Arrays.copyOfRange(postorder,0,index));
        if(index != inorder.length - 1)
            t.right=buildTree(Arrays.copyOfRange(inorder,index+1,inorder.length), Arrays.copyOfRange(postorder,index,postorder.length-1));
        return t;
    }
}  