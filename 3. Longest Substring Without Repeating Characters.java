class Solution {
    public int lengthOfLongestSubstring(String s) 
    {
        int result = 0;
        int start = 0;
        Set<Character> set = new HashSet<Character>();
        for(int i = 0; i < s.length(); i++)
        {
            if(!set.contains(s.charAt(i)))
            {
                set.add(s.charAt(i));
                result = Math.max(result, i - start + 1);
            }
            else
            {
                while(set.contains(s.charAt(i)))
                    set.remove(s.charAt(start++));
                 set.add(s.charAt(i));
            }
        }
        return result;
    }
}