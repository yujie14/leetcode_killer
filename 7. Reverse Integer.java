class Solution {
    public int reverse(int x) 
    {
        if(x == 0)
            return 0;
        boolean flag = (x < 0);
        int result = 0;
        x = Math.abs(x);
        while(x != 0)
        {
            int temp = 10*result + x%10;
            if((temp - x%10)/10 != result)
                return 0;
            x = x/10;
            result = temp;
        }
        return (flag? (-1) : 1) * result;
    }
}