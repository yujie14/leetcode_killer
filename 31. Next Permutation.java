class Solution {
    public void nextPermutation(int[] nums) 
    {
        int pre = nums.length - 2;
        while(pre >= 0 && nums[pre] >= nums[pre + 1])
            pre--;
        if(pre == -1)
        {
            reverse(nums, 0, nums.length - 1);
            return;
        }
        int i = nums.length - 1;
        while(nums[i] <= nums[pre])
            i--;
        swap(nums, i, pre);
        reverse(nums, pre + 1, nums.length - 1);
    }
    public void swap(int[] nums, int i, int j)
    {
        int a = nums[i];
        nums[i] = nums[j];
        nums[j] = a;
    }
    public void reverse(int[] nums, int i, int j)
    {
        while(i < j)
            swap(nums, i++, j--);
    }
}