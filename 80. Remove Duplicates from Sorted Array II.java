class Solution {
    public int removeDuplicates(int[] nums) 
    {
        if(nums.length <= 1)
            return nums.length;
        int count = 1;
        boolean flag = false;
        for(int i = 1; i < nums.length; i++)
        {
            if(nums[i] == nums[i-1] && !flag)
            {
                flag = true;
                nums[count++] = nums[i];
            }
            else if(nums[i] == nums[i-1] && flag);
            else if(nums[i] != nums[i-1])
            {
                flag = false;
                nums[count++] = nums[i];
            }
        }
        return count;
    }
}