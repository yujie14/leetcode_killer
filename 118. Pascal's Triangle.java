class Solution {
    public List<List<Integer>> generate(int numRows) 
    {
        if(numRows <= 0)
            return new ArrayList();
        List<List<Integer>> result = new ArrayList();
        List<Integer> l1 = new ArrayList();
        l1.add(1);
        result.add(l1);
        if(numRows == 1)
            return result;
        for(int i = 2; i <= numRows; i++)
        {
            List<Integer> l = result.get(result.size() - 1);
            List<Integer> lnew = new ArrayList();
            lnew.add(1);
            if(l.size() > 1)
                for(int j = 1; j < l.size(); j++)
                    lnew.add(l.get(j-1) + l.get(j));
            lnew.add(1);
            result.add(lnew);
        }
        return result;       
    }
}