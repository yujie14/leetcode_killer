/**
 * Definition for singly-linked list.
 * public class ListNode {
 *     int val;
 *     ListNode next;
 *     ListNode(int x) { val = x; }
 * }
 */
class Solution {
    public ListNode addTwoNumbers(ListNode l1, ListNode l2) 
    {
        int c = 0;
        int s = 0;
        ListNode start = new ListNode(0);
        ListNode n = start;
        while(l1 != null && l2 != null)
        {
            s = l1.val + l2.val + c;
            c = s/10;
            n.next = new ListNode(s % 10);
            n = n.next;
            l1 = l1.next;
            l2 = l2.next;
        }
        while(l1 != null)
        {
            s = l1.val + c;
            c = s/10;
            n.next = new ListNode(s % 10);
            n = n.next;
            l1 = l1.next;
        }
        while(l2 != null)
        {
            s = l2.val + c;
            c = s/10;
            n.next = new ListNode(s % 10);
            n = n.next;
            l2 = l2.next;
        }
        if(c != 0)
            n.next = new ListNode(c);
        return start.next;
    }
}