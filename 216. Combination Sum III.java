class Solution {
    public List<List<Integer>> combinationSum3(int k, int n) 
    {
        List<List<Integer>> result = new ArrayList();
        helper(result, new ArrayList(), k, n, 1);
        return result;
    }
    public void helper(List<List<Integer>> result, List temp, int k, int n, int start)
    {
        if(k == 0 && n == 0)
            result.add(new ArrayList(temp));
        else if((k == 0 && n < 0)||(k > 0 && n == 0));
        else
        {
            for(int i = start; i <= 9; i++)
            {
                temp.add(i);
                helper(result, temp, k-1, n-i, i+1);
                temp.remove(temp.size()-1);
            }
        }
    }
}