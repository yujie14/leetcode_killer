class Solution {
    public List<String> findMissingRanges(int[] nums, int lower, int upper) 
    {
        List<String> result = new ArrayList();
        if(nums.length == 0)
        {
            if(lower == upper)
                result.add(lower + "");
            else
                result.add(lower + "->" + upper);
            return result;
        }
        if(nums[0] == lower + 1)
            result.add(lower+"");
        else if(nums[0] > lower)
            result.add(lower + "->" + (nums[0]-1));
        for(int i = 1; i < nums.length; i++)
        {
            if(nums[i] == nums[i-1] + 2)
                result.add((nums[i-1]+1) + "");
            else if(nums[i] == nums[i-1] || nums[i] == 1 + nums[i-1]);
            else
                result.add((nums[i-1]+1) + "->" + (nums[i]-1));
        }
        if(nums[nums.length-1] + 1 == upper)
            result.add(upper + "");
        else if(nums[nums.length-1] < upper)
            result.add((nums[nums.length-1]+1) + "->" + upper);
        return result;
            
    }
}