class Solution {
    public boolean isValid(String s) 
    {
        if(s.length() == 0)
            return true;
        Stack<Integer> st = new Stack();
        for(int i = 0; i < s.length(); i++)
        {
            char c = s.charAt(i);
            if(st.empty() && (c == ')' || c == ']' || c == '}'))
                return false;
            if(c == '(')
                st.push(1);
            else if(c == '[')
                st.push(2);
            else if(c == '{')
                st.push(3);
            else if(c == ')')
            {
                if(st.pop() == 1);
                else
                    return false;
            }
            else if(c == ']')
            {
                if(st.pop() == 2);
                else
                    return false;
            }
            else if(c == '}')
            {
                if(st.pop() == 3);
                else
                    return false;
            }
        }
        return st.empty();
    }
}