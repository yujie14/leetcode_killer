class Solution {
    public int shortestDistance(String[] words, String word1, String word2) 
    {
        int result = Integer.MAX_VALUE;
        int a = 10000;
        int b = -10000;
        for(int i = 0; i < words.length; i++)
        {
            if(words[i].equals(word1))
                a = i;
            if(words[i].equals(word2))
                b = i;
            result = Math.min(result, Math.abs(b-a));
        }
        return result;
    }
}