class Solution {
    public boolean exist(char[][] board, String word) 
    {
        boolean flag = false;
        if(word.length() == 0 || board.length == 0 || board[0].length == 0)
            return false;
        boolean[][] visited = new boolean[board.length][board[0].length];
        char f = word.charAt(0);
        for(int i = 0; i < board.length; i++)
            for(int j = 0; j < board[0].length; j++)
                if(f == board[i][j])
                    flag = flag || helper(board, visited, i, j, word, 0);
        return flag;
    }
    public boolean helper(char[][] board, boolean[][] visited, int i, int j, String word, int num)
    {
        if(num == word.length())
            return true;
        boolean flag = false;
        if(i>=0&&i<board.length&&j>=0&&j<board[0].length&&!visited[i][j]&&board[i][j]==word.charAt(num))
        {
            visited[i][j] = true;
            flag = flag || helper(board, visited, i-1, j, word, num+1);
            flag = flag || helper(board, visited, i+1, j, word, num+1);
            flag = flag || helper(board, visited, i, j-1, word, num+1);
            flag = flag || helper(board, visited, i, j+1, word, num+1);
            visited[i][j] = false;
        }
        return flag;
    }
}