class Solution {
    public int shortestWordDistance(String[] words, String word1, String word2) 
    {
        int result = Integer.MAX_VALUE;
        int j = 0;
        for(int i = 0; i < words.length; i++)
        {
            if(i != j && words[i].equals(word1) && words[j].equals(word2))
                result = Math.min(Math.abs(i - j), result);
            if(i != j && words[i].equals(word2) && words[j].equals(word1))
                result = Math.min(Math.abs(i - j), result);
            if(words[i].equals(word1) || words[i].equals(word2))
                j = i;
        }
        return result;
    }
}