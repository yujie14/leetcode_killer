class Solution {
    public int[] getModifiedArray(int length, int[][] updates) 
    {
        int[] result = new int[length];
        for(int i = 0; i < updates.length; i++)
        {
            result[updates[i][0]] += updates[i][2];
            if(updates[i][1] < length - 1)
                result[updates[i][1]+1] -= updates[i][2];
        }
        for(int i = 1; i < result.length; i++)
            result[i] = result[i-1] + result[i];
        return result;
    }
}