/**
 * Definition for a binary tree node.
 * public class TreeNode {
 *     int val;
 *     TreeNode left;
 *     TreeNode right;
 *     TreeNode(int x) { val = x; }
 * }
 */
class Solution {
    public int search(int[] arr, int target)
    {
        for(int i = 0; i < arr.length; i++)
            if(arr[i] == target)
                return i;
        return -1;
    }
    public TreeNode buildTree(int[] preorder, int[] inorder) 
    {
        if(preorder.length == 0 || inorder.length == 0)
            return null;
        int index = search(inorder, preorder[0]);
        TreeNode t = new TreeNode(preorder[0]);
        if(index == -1)
            return null;
        if(index != 0)
            t.left=buildTree(Arrays.copyOfRange(preorder,1,index+1),Arrays.copyOfRange(inorder,0,index));
        if(index != inorder.length - 1)
            t.right=buildTree(Arrays.copyOfRange(preorder,index+1,preorder.length),Arrays.copyOfRange(inorder,index+1,inorder.length));
        return t;
    }
}