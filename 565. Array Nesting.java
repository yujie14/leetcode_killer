class Solution {
    public int arrayNesting(int[] nums) 
    {
        int result = 0;
        if(nums.length == 0)
            return result;
        Set<Integer> set = new HashSet();
        for(int i = 0; i < nums.length; i++)
        {
            if(!set.contains(nums[i]))
            {
                int count = 0;
                Set<Integer> temp = new HashSet();
                int next = nums[i];
                while(!temp.contains(next))
                {
                    temp.add(next);
                    count++;
                    next = nums[next];
                }
                result = Math.max(result, count);
                set.addAll(temp);
            }
            else
                continue;
        }
        return result;
    }
}