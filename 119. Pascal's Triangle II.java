class Solution {
    public List<Integer> getRow(int rowIndex) 
    {
        if(rowIndex < 0)
            return new ArrayList();
        List<Integer> previous = new ArrayList();
        List<Integer> current = new ArrayList();
        if(rowIndex == 0)
        {
            previous.add(1);
            return previous;
        }
        if(rowIndex == 1)
        {
            previous.add(1);
            previous.add(1);
            return previous;
        }
        previous.add(1);
        previous.add(1);
        for(int i = 2; i <= rowIndex; i++)
        {
            current.add(1);
            for(int j = 1; j < previous.size(); j++)
                current.add(previous.get(j-1) + previous.get(j));
            current.add(1);
            previous = new ArrayList(current);
            current.clear();
        }
        return previous;
    }
}