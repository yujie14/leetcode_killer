class Solution {
    public List<List<Integer>> fourSum(int[] nums, int target) 
    {
        List l = new ArrayList();
        Arrays.sort(nums);
        for(int i = 0; i < nums.length - 3; i++)
        {
            for(int s = nums.length - 1; s > i; s--)
            {
                if(i > 0 && nums[i] == nums[i-1])
                    continue;
                if(s < nums.length - 1 && nums[s] == nums[s+1])
                    continue;
                int j = i + 1, k = s - 1;
                while(j < k)
                {
                    if(nums[i] + nums[j] + nums[k] + nums[s]==target)
                    {
                        List l1 = new ArrayList();
                        l1.add(nums[i]);
                        l1.add(nums[j]);
                        l1.add(nums[k]);
                        l1.add(nums[s]);
                        l.add(l1);
                        j++;
                        k--;
                        while(j < k && nums[j]==nums[j-1])  j++;
                        while(j < k && nums[k]==nums[k+1])  k--;
                    }
                    else if(nums[i] + nums[j] + nums[k] + nums[s]< target)
                        j++;
                    else
                        k--;
                }
            }
        }
        return l;
    }
}