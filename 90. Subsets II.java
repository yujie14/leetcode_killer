class Solution {
    public List<List<Integer>> subsetsWithDup(int[] nums) 
    {
        Arrays.sort(nums);
        List result = new ArrayList();
        helper(result, new ArrayList(), nums, 0);
        return result;
    }  
    public void helper(List result, List temp, int[] nums, int start)
    {
        result.add(new ArrayList(temp));
        for(int i = start; i < nums.length; i++)
        {
            temp.add(nums[i]);
            helper(result, temp, nums, i + 1);
            temp.remove(temp.size() - 1);
            while(i != nums.length-1 && nums[i] == nums[i+1])
                i++;
        }
    }
}