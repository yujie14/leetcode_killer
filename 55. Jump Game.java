class Solution {
    public boolean canJump(int[] nums) 
    {
        int count = 1;
        for(int i = 0; i < nums.length; i++)
        {
            if(i == nums.length - 1)
                return true;
            count = Math.max(count-1, nums[i]);
            if(count == 0)
                return false;
        }
        return true;
    }
}